// CaptureDevicesAIDL.aidl
package com.pds.bistrov2;

// Declare any non-default types here with import statements

interface CaptureDevicesCallback {
    void pushNotificationCallback(String authCode, String extRef, String description, int saleIntentStatus);
}