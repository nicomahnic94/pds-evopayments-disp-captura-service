// CaptureDevicesAIDL.aidl
package com.pds.evopayments_disp_captura_service;

// Declare any non-default types here with import statements

interface CaptureDevicesAIDL {
    String getPaymentStatus(String extRef, String androidId);

    String createSaleIntent(double amount, String orderId, String androidId);

    String cancelSaleIntent(String extRef, String androidId);
}