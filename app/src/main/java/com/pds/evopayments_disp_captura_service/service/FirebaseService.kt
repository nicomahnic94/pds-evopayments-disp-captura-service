package com.pds.evopayments_disp_captura_service.service

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.pds.evopayments_disp_captura_service.domain.models.response.PushResponse
import com.pds.evopayments_disp_captura_service.helpers.SharedPrefs
import java.util.*

class FirebaseService: FirebaseMessagingService() {

    private val TAG = "FirebaseService"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.e(TAG, "onMessageReceived..... ")
        if (remoteMessage.data.isNotEmpty()) {
            Log.e(TAG, "NM onMessageReceived -> ${remoteMessage.data} ")
            val push = PushResponse(
                authCode = remoteMessage.data["authCode"],
                extRef = remoteMessage.data["externalReferenceId"]!!,
                description = remoteMessage.data["description"],
                status = remoteMessage.data["status"]!!.toInt()
            )
            val serviceInstalled: Boolean = CallbackConnector.instance!!.connectToRemoteService(this, push)
            Log.e(TAG, "NM serviceInstalled -> $serviceInstalled")
        }
    }

    override fun onNewToken(registrationId: String) {
        super.onNewToken(registrationId)
        Log.e(TAG, "onNewToken: $registrationId")
    }


    override fun onDeletedMessages() {
        super.onDeletedMessages()
        Log.e(TAG, "onDeletedMessages: ")
    }

}