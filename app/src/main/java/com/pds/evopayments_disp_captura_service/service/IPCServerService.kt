package com.pds.evopayments_disp_captura_service.service

import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import com.google.gson.Gson
import com.pds.evopayments_disp_captura_service.CaptureDevicesAIDL
import com.pds.evopayments_disp_captura_service.domain.models.request.CancelSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.request.CreateSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.request.StatusRequest
import com.pds.evopayments_disp_captura_service.domain.models.response.CancelSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.CreateSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.StatusPaymentResponse
import com.pds.evopayments_disp_captura_service.domain.repository.CancelSaleIntent
import com.pds.evopayments_disp_captura_service.domain.repository.CreateSaleIntent
import com.pds.evopayments_disp_captura_service.domain.repository.GetPaymentStatus
import com.pds.evopayments_disp_captura_service.helpers.SharedPrefs
import com.pds.evopayments_disp_captura_service.model.Bistro
import com.pds.evopayments_disp_captura_service.utils.DataState
import com.pds.evopayments_disp_captura_service.utils.StatusResponseCode
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject


@AndroidEntryPoint
class IPCServerService : Service() {

    @Inject lateinit var createSaleIntent: CreateSaleIntent
    @Inject lateinit var cancelSaleIntent: CancelSaleIntent
    @Inject lateinit var getPaymentStatus: GetPaymentStatus

    var result: String? = null

    private val serviceJob = Job()
    private val serviceScope = CoroutineScope(Dispatchers.Main + serviceJob)

    companion object {
        // How many connection requests have been received since the service started
        var connectionCount: Int = 0
        // Client might have sent an empty data
        const val NOT_SENT = "Not sent!"
        const val FAILURE_MESSAGE = "Tuvimos un problema al realizar la operación, comuníquese con soporte técnico. Cod Error #500"
        const val FAILURE_MESSAGE_2 = "Tuvimos un problema al realizar la operación, comuníquese con soporte técnico. Cod Error #501"
    }

    // AIDL IPC - Binder object to pass to the client
    @ExperimentalCoroutinesApi
    private val captureDeviceBinder = object : CaptureDevicesAIDL.Stub() {

        override fun getPaymentStatus(extRef: String, androidId: String): String {
            val req = StatusRequest(androidId, SharedPrefs.token, extRef)
            Log.d("NM", "Request               AndroidId -> ${req.androidId}")
            Log.d("NM", "Request               RegId -> ${req.regId}")
            Log.d("NM", "Request               ExtRef -> ${req.extRefId}")

            serviceScope.async {
                apiGetRequest(req)
            }
            while(result == null){
                Thread.sleep(10)
            }
            return result!!
        }

        override fun createSaleIntent(amount: Double, orderId: String, androidId: String): String {
            SharedPrefs.orderId = orderId
            val req = CreateSaleIntentRequest(androidId,  SharedPrefs.token, amount, orderId)
            Log.d("NM", "Request               AndroidId -> ${req.androidId}")
            Log.d("NM", "Request               RegId -> ${req.regId}")
            Log.d("NM", "Request               Amount -> ${req.amount}")
            Log.d("NM", "Request               OrderId -> ${req.orderId}")

            serviceScope.async {
                apiPostRequest(req)
            }
            while(result == null){
                Thread.sleep(10)
            }
            return result!!
        }

        override fun cancelSaleIntent(extRef: String, androidId: String): String {
            val androidID = androidId
            val req = CancelSaleIntentRequest(androidID, SharedPrefs.token, extRef)
            Log.d("NM", "Request               AndroidId -> ${req.androidId}")
            Log.d("NM", "Request               RegId -> ${req.regId}")
            Log.d("NM", "Request               ExtRef -> ${req.extRefId}")

            serviceScope.async {
                apiDeleteRequest(req)
            }
            while(result == null){
                Thread.sleep(10)
            }
            return result!!
        }
    }

    // Pass the binder object to clients so they can communicate with this service
    override fun onBind(intent: Intent?): IBinder? {
        // Choose which binder we need to return based on the type of IPC the client makes
        return when (intent?.action) {
            "captureDeviceBinder" -> captureDeviceBinder
            else -> null
        }
    }

    // A client has unbound from the service
    override fun onUnbind(intent: Intent?): Boolean {
        Bistro.client = null
        return super.onUnbind(intent)
    }

    @ExperimentalCoroutinesApi
    private suspend fun apiPostRequest(req: CreateSaleIntentRequest){
        createSaleIntent.postResquest(req)
            .catch { exception -> Log.d("NM","exception -> $exception") }
            .onEach { res ->
                when(res) {
                    is DataState.Success -> {
                        Log.d("NM", "POST DataState -> SUCCESS")
                        if(res.data.responseCode == StatusResponseCode.INVALID_NOT_CATCHED) res.data.responseMessage = FAILURE_MESSAGE_2
                        result = Gson().toJson(res.data)
                    }
                    is DataState.Failure -> {
                        Log.d("NM", "POST DataState -> FAILURE")
                        val failRes = CreateSaleIntentResponse(
                            responseCode = -1,
                            responseMessage = FAILURE_MESSAGE
                        )
                        result = Gson().toJson(failRes)
                    }
                    is DataState.Loading -> {
                        Log.d("NM", "POST DataState -> LOADING")
                    }
                }
            }.launchIn(serviceScope)
    }

    @ExperimentalCoroutinesApi
    private fun apiDeleteRequest(req: CancelSaleIntentRequest){
        serviceScope.launch {
            cancelSaleIntent.deleteResquest(req)
                .catch { exception -> Log.d("NM","exception -> $exception") }
                .onEach { res ->
                    when(res) {
                        is DataState.Success -> {
                            Log.d("NM", "DELENTE DataState -> SUCCESS")
                            result = Gson().toJson(res.data)
                        }
                        is DataState.Failure -> {
                            Log.d("NM", "DELENTE DataState -> FAILURE")
                            val failRes = CancelSaleIntentResponse(
                                responseCode = -2,
                                responseMessage = FAILURE_MESSAGE
                            )
                            result = Gson().toJson(failRes)
                        }
                        is DataState.Loading -> {
                            Log.d("NM", "DELENTE DataState -> LOADING")
                        }
                    }
                }.launchIn(serviceScope)
        }
    }

    @ExperimentalCoroutinesApi
    private fun apiGetRequest(req: StatusRequest){
        serviceScope.launch {
            getPaymentStatus.getResquest(req)
                .catch { exception -> Log.d("NM","exception -> $exception") }
                .onEach { res ->
                    when(res) {
                        is DataState.Success -> {
                            Log.d("NM", "GET DataState -> SUCCESS")
                            result = Gson().toJson(res.data)
                        }
                        is DataState.Failure -> {
                            Log.d("NM", "GET DataState -> FAILURE")
                            val failRes = StatusPaymentResponse(
                                responseCode = -2,
                                responseMessage = FAILURE_MESSAGE,
                                saleIntentStatus =  -1,
                                authCode = "",
                                description = ""
                            )
                            result = Gson().toJson(failRes)
                        }
                        is DataState.Loading -> {
                            Log.d("NM", "GET DataState -> LOADING")
                        }
                    }
                }.launchIn(serviceScope)
        }
    }

}