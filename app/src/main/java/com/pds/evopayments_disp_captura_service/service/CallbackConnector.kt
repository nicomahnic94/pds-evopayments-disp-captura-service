package com.pds.evopayments_disp_captura_service.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.pds.bistrov2.CaptureDevicesCallback
import com.pds.evopayments_disp_captura_service.domain.models.response.PushResponse

class CallbackConnector(): ServiceConnection {

    var iRemoteService: CaptureDevicesCallback? = null
    private lateinit var context: Context
    private lateinit var pushResponse: PushResponse

    companion object {
        const val TAG = "Device_MPPlus_Callback_Connector"
        private var mInstance: CallbackConnector? = null
        val instance: CallbackConnector?
            get() {
                if (mInstance == null) mInstance = CallbackConnector()
                return mInstance
            }
    }

    override fun onServiceConnected(p0: ComponentName?, service: IBinder?) {
        iRemoteService = CaptureDevicesCallback.Stub.asInterface(service)
        iRemoteService?.pushNotificationCallback(
            pushResponse.authCode ?: "",
            pushResponse.extRef ?: "",
            pushResponse.description ?: "",
            pushResponse.status
        )
        context.applicationContext?.unbindService(this@CallbackConnector)
    }

    override fun onServiceDisconnected(p0: ComponentName?) {
        iRemoteService = null
    }

    fun connectToRemoteService(c: Context, response: PushResponse): Boolean {
        pushResponse = response
        context = c
        Log.e(TAG, "NM connectToRemoteService()")
        val intent = Intent("captureDeviceCallbackBinder")
        val pack = CaptureDevicesCallback::class.java.`package`
        pack?.let {
            intent.setPackage(pack.name)
            return context.applicationContext?.bindService(intent, this, Context.BIND_AUTO_CREATE)!!
        }
        return false
    }
}