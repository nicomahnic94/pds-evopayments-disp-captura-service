package com.pds.evopayments_disp_captura_service.utils

// Bundle keys
object Constants {

    const val BASE_URL = "https://mercadopago.bistrosoft.com"

}

object StatusResponseCode {
    const val VALID = 0
    const val INVALID_NOT_CATCHED = -1
    const val INVALID_CATCHED = -2
}