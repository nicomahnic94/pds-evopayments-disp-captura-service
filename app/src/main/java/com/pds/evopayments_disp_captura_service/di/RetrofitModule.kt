package com.pds.evopayments_disp_captura_service.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pds.evopayments_disp_captura_service.api.*
import com.pds.evopayments_disp_captura_service.domain.models.request.CancelSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.request.CreateSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.request.StatusRequest
import com.pds.evopayments_disp_captura_service.domain.models.response.CancelSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.CreateSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.StatusPaymentResponse
import com.pds.evopayments_disp_captura_service.utils.Constants
import com.pds.evopayments_disp_captura_service.utils.EntityMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RetrofitModule {

    @Singleton
    @Provides
    fun provideCreateResponseMapper(): EntityMapper<CreatePaymentResponseNetworkEntity, CreateSaleIntentResponse> = CreateResponseMapper()

    @Singleton
    @Provides
    fun provideCancelResponseMapper(): EntityMapper<CancelPaymentResponseNetworkEntity, CancelSaleIntentResponse> = CancelResponseMapper()


    @Singleton
    @Provides
    fun provideStatusResponseMapper(): EntityMapper<StatusPaymentResponseNetworkEntity, StatusPaymentResponse> = StatusResponseMapper()

    @Singleton
    @Provides
    fun provideCreateRequestMapper(): EntityMapper<CreatePaymentNetworkEntity, CreateSaleIntentRequest> = CreateRequestMapper()

    @Singleton
    @Provides
    fun provideCancelRequestMapper(): EntityMapper<DeletePaymentNetworkEntity, CancelSaleIntentRequest> = CancelRequestMapper()

    @Singleton
    @Provides
    fun provideStatusRequestMapper(): EntityMapper<StatusPaymentNetworkEntity, StatusRequest> = StatusRequestMapper()

    @Provides
    fun provideBaseUrl() = Constants.BASE_URL

    @Singleton
    @Provides
    fun provideGsonBuilder(): Gson {
        return GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .callTimeout(1, TimeUnit.MINUTES)
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient.Builder): Retrofit {
        return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiHelper: ApiHelperImpl): ApiHelper = apiHelper

}