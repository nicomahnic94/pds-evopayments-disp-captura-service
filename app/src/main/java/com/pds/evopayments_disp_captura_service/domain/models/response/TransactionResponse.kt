package com.pds.evopayments_disp_captura_service.domain.models.response

abstract class TransactionResponse {
    abstract val responseCode: Int
    abstract val responseMessage: String
}

