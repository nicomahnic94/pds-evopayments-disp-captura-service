package com.pds.evopayments_disp_captura_service.domain.models.request

abstract class TransactionRequest {
    abstract val androidId: String
    abstract val regId: String
}