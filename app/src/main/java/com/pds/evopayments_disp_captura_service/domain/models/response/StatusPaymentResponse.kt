package com.pds.evopayments_disp_captura_service.domain.models.response

data class StatusPaymentResponse(

    override val responseCode: Int,
    override val responseMessage: String,
    val authCode: String?,//"123456",
    val description : String?, //"PAYWAVE VISA",
    val saleIntentStatus: Int?// 1

) : TransactionResponse()