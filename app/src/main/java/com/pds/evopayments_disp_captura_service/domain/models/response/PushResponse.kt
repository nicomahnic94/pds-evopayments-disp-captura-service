package com.pds.evopayments_disp_captura_service.domain.models.response

data class PushResponse(
    val authCode: String?,//"123456",
    val extRef: String?,// 2,
    val description : String?, //"PAYWAVE VISA",
    val status: Int// 1
)
