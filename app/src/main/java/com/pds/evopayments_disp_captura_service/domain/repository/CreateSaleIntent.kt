package com.pds.evopayments_disp_captura_service.domain.repository

import android.util.Log
import com.pds.evopayments_disp_captura_service.api.ApiHelper
import com.pds.evopayments_disp_captura_service.api.CreateRequestMapper
import com.pds.evopayments_disp_captura_service.api.CreateResponseMapper
import com.pds.evopayments_disp_captura_service.domain.models.request.CreateSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.response.CreateSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.TransactionResponse
import com.pds.evopayments_disp_captura_service.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class CreateSaleIntent @Inject constructor(
        private val apiHelper: ApiHelper,
        private val txnResponseMapper: CreateResponseMapper,
        private val txnRequestMapper: CreateRequestMapper
) {

    suspend fun postResquest(req: CreateSaleIntentRequest): Flow<DataState<CreateSaleIntentResponse>> = flow {
//        emit(DataState.Loading)

        try{
            val res = apiHelper.postRequest(txnRequestMapper.mapToEntity(req))
            Log.d("NM", "Create Response              ResCode -> ${res.responseCode}")
            Log.d("NM", "Create Response              ResMessage -> ${res.responseMessage}")
            Log.d("NM", "Create Response              ExternalRefId -> ${res.extRefId}")
            emit(DataState.Success(txnResponseMapper.mapFromEntity(res)))
        } catch (e: Exception){
            Log.d("NM","postReq -> FAILURE ${e}")
            emit(DataState.Failure(e))
        }
    }

}