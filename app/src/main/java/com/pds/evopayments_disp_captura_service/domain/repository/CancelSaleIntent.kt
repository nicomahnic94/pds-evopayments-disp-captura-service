package com.pds.evopayments_disp_captura_service.domain.repository

import android.util.Log
import com.pds.evopayments_disp_captura_service.api.ApiHelper
import com.pds.evopayments_disp_captura_service.api.CancelRequestMapper
import com.pds.evopayments_disp_captura_service.api.CancelResponseMapper
import com.pds.evopayments_disp_captura_service.domain.models.request.CancelSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.response.CancelSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.TransactionResponse
import com.pds.evopayments_disp_captura_service.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class CancelSaleIntent @Inject constructor(
        private val apiHelper: ApiHelper,
        private val txnResponseMapper: CancelResponseMapper,
        private val txnRequestMapper: CancelRequestMapper
) {

    suspend fun deleteResquest(req: CancelSaleIntentRequest): Flow<DataState<CancelSaleIntentResponse>> = flow {
//        emit(DataState.Loading)

        try{
            val res = apiHelper.deleteRequest(txnRequestMapper.mapToEntity(req))
            Log.d("NM", "Cancel Response              ResCode -> ${res.responseCode}")
            Log.d("NM", "Cancel Response              ResMessage -> ${res.responseMessage}")
            emit(DataState.Success(txnResponseMapper.mapFromEntity(res)))
        } catch (e: Exception){
            Log.d("NM","postReq -> FAILURE ${e}")
            emit(DataState.Failure(e))
        }
    }

}