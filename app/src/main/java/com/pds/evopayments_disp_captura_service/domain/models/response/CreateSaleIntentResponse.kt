package com.pds.evopayments_disp_captura_service.domain.models.response

data class CreateSaleIntentResponse(

    override val responseCode: Int,
    override var responseMessage: String,
    val extRefId: String? = null

) : TransactionResponse()
