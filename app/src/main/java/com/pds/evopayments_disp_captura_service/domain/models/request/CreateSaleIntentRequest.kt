package com.pds.evopayments_disp_captura_service.domain.models.request

data class CreateSaleIntentRequest (
        override val androidId: String,
        override val regId: String,
        val amount: Double,
        val orderId: String
) : TransactionRequest()