package com.pds.evopayments_disp_captura_service.domain.models.request

data class CancelSaleIntentRequest (
        override val androidId: String,
        override val regId: String,
        val extRefId: String
) : TransactionRequest()