package com.pds.evopayments_disp_captura_service.domain.models.response

data class CancelSaleIntentResponse(

    override val responseCode: Int,
    override val responseMessage: String

) : TransactionResponse()