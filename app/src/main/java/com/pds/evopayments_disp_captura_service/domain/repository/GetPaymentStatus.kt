package com.pds.evopayments_disp_captura_service.domain.repository

import android.util.Log
import com.pds.evopayments_disp_captura_service.api.ApiHelper
import com.pds.evopayments_disp_captura_service.api.StatusRequestMapper
import com.pds.evopayments_disp_captura_service.api.StatusResponseMapper
import com.pds.evopayments_disp_captura_service.domain.models.request.StatusRequest
import com.pds.evopayments_disp_captura_service.domain.models.response.StatusPaymentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.TransactionResponse
import com.pds.evopayments_disp_captura_service.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class GetPaymentStatus @Inject constructor(
        private val apiHelper: ApiHelper,
        private val txnResponseMapper: StatusResponseMapper,
        private val txnRequestMapper: StatusRequestMapper
) {

    suspend fun getResquest(req: StatusRequest): Flow<DataState<StatusPaymentResponse>> = flow {
//        emit(DataState.Loading)

        try{
            val res = apiHelper.getRequest(txnRequestMapper.mapToEntity(req))
            Log.d("NM", "Status Response              ResCode -> ${res.responseCode}")
            Log.d("NM", "Status Response              ResMessage -> ${res.responseMessage}")
            Log.d("NM", "Status Response              SaleIntentStatus -> ${res.saleIntentStatus}")
            Log.d("NM", "Status Response              AuthCode -> ${res.authCode}")
            Log.d("NM", "Status Response              Description -> ${res.description}")
            emit(DataState.Success(txnResponseMapper.mapFromEntity(res)))
        } catch (e: Exception){
            Log.d("NM","postReq -> FAILURE ${e}")
            emit(DataState.Failure(e))
        }
    }

}