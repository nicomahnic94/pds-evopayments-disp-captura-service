package com.pds.evopayments_disp_captura_service.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

abstract class TxnResponseNetworkEntity {
        abstract val responseCode: Int
        abstract val responseMessage: String
}

data class StatusPaymentResponseNetworkEntity(

        @SerializedName("saleIntentStatus")
        @Expose
        val saleIntentStatus: Int?,

        @SerializedName("authCode")
        @Expose
        val authCode: String?,

        @SerializedName("description")
        @Expose
        val description: String?,

        @SerializedName("responseCode")
        @Expose
        override val responseCode: Int,

        @SerializedName("responseMessage")
        @Expose
        override val responseMessage: String

) : TxnResponseNetworkEntity()


data class CreatePaymentResponseNetworkEntity (

        @SerializedName("responseCode")
        @Expose
        override val responseCode: Int,

        @SerializedName("responseMessage")
        @Expose
        override val responseMessage: String,

        @SerializedName("externalReferenceId")
        @Expose
        val extRefId: Int?

) : TxnResponseNetworkEntity()


data class CancelPaymentResponseNetworkEntity (

        @SerializedName("responseCode")
        @Expose
        override val responseCode: Int,

        @SerializedName("responseMessage")
        @Expose
        override val responseMessage: String

) : TxnResponseNetworkEntity()