package com.pds.evopayments_disp_captura_service.api

interface ApiHelper {

    suspend fun postRequest(req: CreatePaymentNetworkEntity) : CreatePaymentResponseNetworkEntity

    suspend fun deleteRequest(req: DeletePaymentNetworkEntity) : CancelPaymentResponseNetworkEntity

    suspend fun getRequest(req: StatusPaymentNetworkEntity) : StatusPaymentResponseNetworkEntity

}