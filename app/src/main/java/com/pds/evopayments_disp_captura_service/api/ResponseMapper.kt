package com.pds.evopayments_disp_captura_service.api

import com.pds.evopayments_disp_captura_service.domain.models.response.CancelSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.CreateSaleIntentResponse
import com.pds.evopayments_disp_captura_service.domain.models.response.StatusPaymentResponse
import com.pds.evopayments_disp_captura_service.utils.EntityMapper
import javax.inject.Inject


class StatusResponseMapper @Inject constructor():
    EntityMapper<StatusPaymentResponseNetworkEntity, StatusPaymentResponse> {

    override fun mapFromEntity(entity: StatusPaymentResponseNetworkEntity): StatusPaymentResponse {
        return StatusPaymentResponse(
                responseCode = entity.responseCode,
                responseMessage = entity.responseMessage,
                saleIntentStatus = entity.saleIntentStatus,
                authCode = entity.authCode,
                description = entity.description

        )
    }

    override fun mapToEntity(domainModel: StatusPaymentResponse): StatusPaymentResponseNetworkEntity {
        return StatusPaymentResponseNetworkEntity(
                responseCode = domainModel.responseCode,
                responseMessage = domainModel.responseMessage,
                saleIntentStatus = domainModel.saleIntentStatus,
                authCode = domainModel.authCode,
                description = domainModel.description
        )
    }

}

class CreateResponseMapper @Inject constructor():
        EntityMapper<CreatePaymentResponseNetworkEntity, CreateSaleIntentResponse> {

    override fun mapFromEntity(entity: CreatePaymentResponseNetworkEntity): CreateSaleIntentResponse {
        return CreateSaleIntentResponse(
                responseCode = entity.responseCode,
                responseMessage = entity.responseMessage,
                extRefId = entity.extRefId.toString()
        )
    }

    override fun mapToEntity(domainModel: CreateSaleIntentResponse): CreatePaymentResponseNetworkEntity {
        return CreatePaymentResponseNetworkEntity(
                responseCode = domainModel.responseCode,
                responseMessage = domainModel.responseMessage,
                extRefId = domainModel.extRefId?.toInt()
        )
    }

}

class CancelResponseMapper @Inject constructor():
        EntityMapper<CancelPaymentResponseNetworkEntity, CancelSaleIntentResponse> {

    override fun mapFromEntity(entity: CancelPaymentResponseNetworkEntity): CancelSaleIntentResponse {
        return CancelSaleIntentResponse(
                responseCode = entity.responseCode,
                responseMessage = entity.responseMessage,
        )
    }

    override fun mapToEntity(domainModel: CancelSaleIntentResponse): CancelPaymentResponseNetworkEntity {
        return CancelPaymentResponseNetworkEntity(
                responseCode = domainModel.responseCode,
                responseMessage = domainModel.responseMessage,
        )
    }

}