package com.pds.evopayments_disp_captura_service.api

import javax.inject.Inject

class ApiHelperImpl @Inject constructor(
    private val apiService: ApiService
): ApiHelper {

    override suspend fun postRequest(req: CreatePaymentNetworkEntity) : CreatePaymentResponseNetworkEntity = apiService.postRequest(req)

    override suspend fun deleteRequest(req: DeletePaymentNetworkEntity) : CancelPaymentResponseNetworkEntity = apiService.deleteRequest(req)

    override suspend fun getRequest(req: StatusPaymentNetworkEntity) : StatusPaymentResponseNetworkEntity = apiService.getRequest(req.extRefId, req.androidID, req.regId)

}