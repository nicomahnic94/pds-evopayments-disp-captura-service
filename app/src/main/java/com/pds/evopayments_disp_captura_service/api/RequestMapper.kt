package com.pds.evopayments_disp_captura_service.api

import com.pds.evopayments_disp_captura_service.domain.models.request.CancelSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.request.CreateSaleIntentRequest
import com.pds.evopayments_disp_captura_service.domain.models.request.StatusRequest
import com.pds.evopayments_disp_captura_service.utils.EntityMapper
import javax.inject.Inject

class StatusRequestMapper @Inject constructor():
    EntityMapper<StatusPaymentNetworkEntity, StatusRequest> {

    override fun mapFromEntity(entity: StatusPaymentNetworkEntity): StatusRequest {
        return StatusRequest(
            androidId = entity.androidID,
            regId = entity.regId,
            extRefId = entity.extRefId.toString()
        )
    }

    override fun mapToEntity(domainModel: StatusRequest): StatusPaymentNetworkEntity {
        return StatusPaymentNetworkEntity(
            androidID = domainModel.androidId,
            regId = domainModel.regId,
            extRefId = domainModel.extRefId.toInt()
        )
    }

}

class CreateRequestMapper @Inject constructor():
        EntityMapper<CreatePaymentNetworkEntity, CreateSaleIntentRequest> {

    override fun mapFromEntity(entity: CreatePaymentNetworkEntity): CreateSaleIntentRequest {
        return CreateSaleIntentRequest(
            androidId = entity.androidID,
            orderId = entity.orderId,
            amount = entity.amount,
            regId = entity.regId
        )
    }

    override fun mapToEntity(domainModel: CreateSaleIntentRequest): CreatePaymentNetworkEntity {
        return CreatePaymentNetworkEntity(
            androidID = domainModel.androidId,
            orderId = domainModel.orderId,
            amount = domainModel.amount,
            regId = domainModel.regId
        )
    }

}

class CancelRequestMapper @Inject constructor():
        EntityMapper<DeletePaymentNetworkEntity, CancelSaleIntentRequest> {

    override fun mapFromEntity(entity: DeletePaymentNetworkEntity): CancelSaleIntentRequest {
        return CancelSaleIntentRequest(
            androidId = entity.androidID,
            regId = entity.regId,
            extRefId = entity.extRefId.toString()
        )
    }

    override fun mapToEntity(domainModel: CancelSaleIntentRequest): DeletePaymentNetworkEntity {
        return DeletePaymentNetworkEntity(
            androidID = domainModel.androidId,
            regId = domainModel.regId,
            extRefId = domainModel.extRefId.toInt()
        )
    }

}