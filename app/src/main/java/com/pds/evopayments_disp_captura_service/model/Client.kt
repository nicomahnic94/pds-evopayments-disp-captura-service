package com.pds.evopayments_disp_captura_service.model

data class Client(
    var clientPackageName: String?,
    var clientProcessId: String?,
    var clientData: String?,
    var ipcMethod: String
)

object Bistro {
    var client: Client? = null
}