package com.pds.evopayments_disp_captura_service.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("StaticFieldLeak")
object SharedPrefs {

    private const val PREFS_NAME = "com.pds.evopayments_disp_captura_service.helpers"
    private const val SHARED_TOKEN = "firebase_push_token"
    private const val SHARED_ORDER_ID = "orderId"
    private lateinit var prefs: SharedPreferences

    var token: String
        get() = prefs.getString(SHARED_TOKEN, "")!!
        set(value) = prefs.edit().putString(SHARED_TOKEN, value).apply()

    var orderId: String
        get() = prefs.getString(SHARED_ORDER_ID, "")!!
        set(value) = prefs.edit().putString(SHARED_ORDER_ID, value).apply()

    fun config (context: Context){
        prefs = context.getSharedPreferences(PREFS_NAME, 0)
    }

}