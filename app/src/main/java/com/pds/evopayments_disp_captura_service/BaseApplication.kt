package com.pds.evopayments_disp_captura_service

import android.app.Application
import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging
import com.pds.evopayments_disp_captura_service.helpers.SharedPrefs
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        SharedPrefs.config(applicationContext)
        FirebaseMessaging.getInstance().token.addOnCompleteListener { token ->
            Log.d("NM", "FIREBASE               TOKEN -> ${token.result}")
            token.result?.let { SharedPrefs.token = it}
        }
    }

}